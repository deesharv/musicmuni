package com.textapp.musicmuniapp

import android.content.Context
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.AsyncTask
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import com.textapp.musicmuniapp.soundtouch.SoundTouch
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    //<editor-fold desc="VARIABLES">
    private var recorder: AudioRecord? = null
    lateinit var audioData: ShortArray
    private var bufferSize = 0
    private var recordingThread: Thread? = null
    private var isRecording = false

    lateinit var bufferData: IntArray
    var bytesRecorded = 0

    // Requesting permission to RECORD_AUDIO
    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(
        android.Manifest.permission.RECORD_AUDIO,
        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        android.Manifest.permission.READ_EXTERNAL_STORAGE
    )
    private var player: MediaPlayer? = null
    var consoleText = StringBuilder()

    //</editor-fold>

    //<editor-fold desc="DEFAULT">
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ActivityCompat.requestPermissions(
            this,
            permissions,
            MyConstants.REQUEST_RECORD_AUDIO_PERMISSION
        )

        btn_start.setOnClickListener { startRecording() }
        //btn_stop.setOnClickListener { stopRecording() }

        bufferSize = AudioRecord.getMinBufferSize(
            MyConstants.RECORDER_SAMPLERATE,
            MyConstants.RECORDER_CHANNELS,
            MyConstants.RECORDER_AUDIO_ENCODING
        ) * 3
        audioData = ShortArray(bufferSize) //short array that pcm data is put into.


        // Check soundtouch library presence & version
        checkLibVersion()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted =
            if (requestCode == MyConstants.REQUEST_RECORD_AUDIO_PERMISSION) {
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            } else {
                false
            }
        if (!permissionToRecordAccepted) finish()
    }

    override fun onStop() {
        super.onStop()
        recorder?.release()
        recorder = null
        player?.release()
        player = null
    }

    object MyConstants {
        const val REQUEST_RECORD_AUDIO_PERMISSION = 200
        const val StartRecord = 0
        const val Recording = 1
        const val Playback = 2

        const val RECORDER_BPP = 16
        const val AUDIO_RECORDER_FILE_EXT_WAV = ".wav"
        const val AUDIO_RECORDER_FOLDER = "AudioRecorderMM"
        const val AUDIO_RECORDER_TEMP_FILE = "mm_recording.raw"
        const val RECORDER_SAMPLERATE = 44100
        val RECORDER_CHANNELS: Int = AudioFormat.CHANNEL_IN_STEREO
        val RECORDER_AUDIO_ENCODING: Int = AudioFormat.ENCODING_PCM_16BIT
    }

    //</editor-fold>

    //<editor-fold desc="RECORD">
    /// print SoundTouch native library version onto console
    protected open fun checkLibVersion() {
        val ver: String? = SoundTouch().getVersionString()
        appendToConsole("SoundTouch native library version = $ver")
    }

    private fun startRecording() {

        recorder = AudioRecord(
            MediaRecorder.AudioSource.MIC,
            MyConstants.RECORDER_SAMPLERATE,
            MyConstants.RECORDER_CHANNELS,
            MyConstants.RECORDER_AUDIO_ENCODING,
            bufferSize
        )

        val i = recorder!!.state
        if (i == 1) recorder!!.startRecording()
        isRecording = true
        recordingThread = Thread({ writeAudioDataToFile() }, "AudioRecorder Thread")
        recordingThread!!.start()

        viewanimator.displayedChild = MyConstants.Recording
        object : CountDownTimer(10000, 100) {
            override fun onTick(millisUntilFinished: Long) {

                var secondsElapsed =
                    ((millisUntilFinished / 1000).toString() + "." + (millisUntilFinished % 1000)).toDouble()
                var showValue: String = DecimalFormat("#.#").format(secondsElapsed).toString()

                txt_timeleft.text = buildSpannedString {
                    bold { append("$showValue\n") }
                    append("seconds")
                }

                progressBar.progress = (millisUntilFinished.toInt()) / 100
            }

            override fun onFinish() {
                if (isRecording) {
                    txt_timeleft.text = "Finished"
                    stopRecording()
                }
            }
        }.start()
    }

    private fun gFilename(): String? {
        val filepath = Environment.getExternalStorageDirectory().path
        val file = File(filepath, MyConstants.AUDIO_RECORDER_FOLDER)
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath.toString() + "/" + System.currentTimeMillis() +
                MyConstants.AUDIO_RECORDER_FILE_EXT_WAV
    }


    private fun gProcessedFilename(existingFileName: String?): String? {
        val filepath = Environment.getExternalStorageDirectory().path
        val file = File(filepath, MyConstants.AUDIO_RECORDER_FOLDER)
        if (!file.exists()) {
            file.mkdirs()
        }
        val existingFile = File(existingFileName)
        return file.absolutePath.toString() + "/" +
                existingFile.name + "_processed" + MyConstants.AUDIO_RECORDER_FILE_EXT_WAV
    }

    private fun gTempFilename(): String? {
        val filepath = Environment.getExternalStorageDirectory().path
        val file = File(filepath, MyConstants.AUDIO_RECORDER_FOLDER)
        if (!file.exists()) {
            file.mkdirs()
        }
        val tempFile = File(filepath, MyConstants.AUDIO_RECORDER_TEMP_FILE)
        if (tempFile.exists())
            tempFile.delete()
        return file.absolutePath.toString() + "/" + MyConstants.AUDIO_RECORDER_TEMP_FILE
    }

    private val tempFilename: String
        private get() {
            val filepath: String = Environment.getExternalStorageDirectory().getPath()
            val file = File(filepath, MyConstants.AUDIO_RECORDER_FOLDER)
            if (!file.exists()) {
                file.mkdirs()
            }
            val tempFile = File(filepath, MyConstants.AUDIO_RECORDER_TEMP_FILE)
            if (tempFile.exists()) tempFile.delete()
            return file.getAbsolutePath()
                .toString() + "/" + MyConstants.AUDIO_RECORDER_TEMP_FILE
        }

    private fun writeAudioDataToFile() {
        val data = ByteArray(bufferSize)
        val filename = gTempFilename()
        var os: FileOutputStream? = null
        try {
            os = FileOutputStream(filename)
        } catch (e: FileNotFoundException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        var read = 0
        if (null != os) {
            while (isRecording) {
                read = recorder!!.read(data, 0, bufferSize)
                if (read > 0) {
                }
                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                    try {
                        os.write(data)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
            try {
                os.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun onRecord(start: Boolean) = if (start) {
        startRecording()
    } else {
        stopRecording()
    }

    private fun stopRecording() {
        if (null != recorder) {
            isRecording = false
            val i = recorder!!.state
            if (i == 1) recorder!!.stop()
            recorder!!.release()
            recorder = null
            recordingThread = null
        }
        txt_timeleft.text = "Finished"

        var fileNameToAccess = gFilename()
        copyWaveFile(gTempFilename(), fileNameToAccess)
        deleteTempFile()

        //***LOGIC***//
        startProcessAndPlay(fileNameToAccess)
        viewanimator.displayedChild = MyConstants.Playback
    }

    /// Function to append status text onto "console box" on the Activity
    fun appendToConsole(text: String?) {
        // run on UI thread to avoid conflicts
        runOnUiThread {
            consoleText.append(text)
            consoleText.append("\n")
            Log.v("Process Status:", consoleText.toString())
        }
    }

    fun startProcessAndPlay(fileNameToAccess: String?) {

        try {
            val task = ProcessTask()
            val params: ProcessTask.Parameters = task.Parameters()
            // parse processing parameters
            params.inFileName = fileNameToAccess
            params.outFileName = gProcessedFilename(fileNameToAccess)
            params.pitch = 6.0f
            params.activity = this

            // update UI about status
            appendToConsole("Process audio file :" + params.inFileName + " => " + params.outFileName)
            appendToConsole("Pitch adjust = " + params.pitch)
//            Toast.makeText(
//                this,
//                "Starting to process file " + params.inFileName + "...",
//                Toast.LENGTH_SHORT
//            ).show()

            // start SoundTouch processing in a background thread
            task.execute(params)
            //task.doSoundTouchProcessing(params);	// this would run processing in main thread

        } catch (exp: java.lang.Exception) {
            exp.printStackTrace()
        }

    }

    private fun deleteTempFile() {
        val file = File(gTempFilename())
        file.delete()
    }


    private fun copyWaveFile(inFilename: String?, outFilename: String?) {
        var inStream: FileInputStream? = null
        var out: FileOutputStream? = null
        var totalAudioLen: Long = 0
        var totalDataLen = totalAudioLen + 36
        val longSampleRate: Long = MyConstants.RECORDER_SAMPLERATE.toLong()
        val channels = 2
        val byteRate: Long =
            MyConstants.RECORDER_BPP * MyConstants.RECORDER_SAMPLERATE * channels / 8.toLong()
        val data = ByteArray(bufferSize)
        try {
            inStream = FileInputStream(inFilename)
            out = FileOutputStream(outFilename)
            totalAudioLen = inStream.getChannel().size()
            totalDataLen = totalAudioLen + 36
            Log.d("File size: ", "$totalDataLen")
            WriteWaveFileHeader(
                out, totalAudioLen, totalDataLen,
                longSampleRate, channels, byteRate
            )
            while (inStream.read(data) !== -1) {
                out.write(data)
            }
            inStream.close()
            out.close()

        } catch (e: Exception) {
            Log.e("Error: ", e.printStackTrace().toString())
        }
    }
    //</editor-fold>

    //<editor-fold desc="PLAY">
    fun startPlaying(filenameAccess: String?) {

        player = MediaPlayer()

        player = MediaPlayer().apply {
            try {
                Log.d("Filename", filenameAccess)
                setDataSource(filenameAccess!!)
                prepare()
                start()
                setOnCompletionListener {
                    viewanimator.displayedChild = MyConstants.StartRecord
                }
            } catch (e: IOException) {
                Log.e("MediaPlayer: ", "prepare() failed")
            }
        }
    }


    private fun stopPlaying() {
        player?.release()
        player = null
    }
    //</editor-fold>

    //<editor-fold desc="SOUNDTOUCH PROCESSING">
    @Throws(IOException::class)
    private fun WriteWaveFileHeader(
        out: FileOutputStream?, totalAudioLen: Long,
        totalDataLen: Long, longSampleRate: Long, channels: Int,
        byteRate: Long
    ) {
        val header = ByteArray(44)
        header[0] = 'R'.toByte() // RIFF/WAVE header
        header[1] = 'I'.toByte()
        header[2] = 'F'.toByte()
        header[3] = 'F'.toByte()
        header[4] = (totalDataLen and 0xff).toByte()
        header[5] = (totalDataLen shr 8 and 0xff).toByte()
        header[6] = (totalDataLen shr 16 and 0xff).toByte()
        header[7] = (totalDataLen shr 24 and 0xff).toByte()
        header[8] = 'W'.toByte()
        header[9] = 'A'.toByte()
        header[10] = 'V'.toByte()
        header[11] = 'E'.toByte()
        header[12] = 'f'.toByte() // 'fmt ' chunk
        header[13] = 'm'.toByte()
        header[14] = 't'.toByte()
        header[15] = ' '.toByte()
        header[16] = 16.toByte() // 4 bytes: size of 'fmt ' chunk
        header[17] = 0.toByte()
        header[18] = 0.toByte()
        header[19] = 0.toByte()
        header[20] = 1.toByte() // format = 1
        header[21] = 0.toByte()
        header[22] = channels.toByte()
        header[23] = 0.toByte()
        header[24] = (longSampleRate and 0xff).toByte()
        header[25] = (longSampleRate shr 8 and 0xff).toByte()
        header[26] = (longSampleRate shr 16 and 0xff).toByte()
        header[27] = (longSampleRate shr 24 and 0xff).toByte()
        header[28] = (byteRate and 0xff).toByte()
        header[29] = (byteRate shr 8 and 0xff).toByte()
        header[30] = (byteRate shr 16 and 0xff).toByte()
        header[31] = (byteRate shr 24 and 0xff).toByte()
        header[32] = (2 * 16 / 8).toByte() // block align
        header[33] = 0.toByte()
        header[34] = MyConstants.RECORDER_BPP.toByte() // bits per sample
        header[35] = 0.toByte()
        header[36] = 'd'.toByte()
        header[37] = 'a'.toByte()
        header[38] = 't'.toByte()
        header[39] = 'a'.toByte()
        header[40] = (totalAudioLen and 0xff).toByte()
        header[41] = (totalAudioLen shr 8 and 0xff).toByte()
        header[42] = (totalAudioLen shr 16 and 0xff).toByte()
        header[43] = (totalAudioLen shr 24 and 0xff).toByte()
        out?.write(header, 0, 44)
    }

    protected class ProcessTask : AsyncTask<ProcessTask.Parameters, Int, Long>() {
        /// Helper class to store the SoundTouch file processing parameters

        inner class Parameters {
            var inFileName: String? = null
            var outFileName: String? = null

            //var tempo = 0f
            var pitch = 0f
            lateinit var activity: Context

        }

        /// Function that does the SoundTouch processing
        public fun doSoundTouchProcessing(params: Parameters): Long {
            val st = SoundTouch()
            //st.setTempo(params.tempo)
            st.setPitchSemiTones(params.pitch)
            Log.i("SoundTouch", "process file " + params.inFileName)

            val startTime = System.currentTimeMillis()
            val res = st.processFile(params.inFileName!!, params.outFileName!!)
            val endTime = System.currentTimeMillis()

            val duration = (endTime - startTime) * 0.001f

            Log.i("SoundTouch", "process file done, duration = $duration")
            (params.activity as MainActivity).appendToConsole("Processing done, duration $duration sec.")

            if (res != 0) {
                val err: String? = SoundTouch.errorString
                (params.activity as MainActivity).appendToConsole("Failure: $err")
                return -1L
            }

            // Play file if so is desirable
            (params.activity as MainActivity).startPlaying(params.outFileName)

            return 0L
        }

        /// Overloaded function that get called by the system to perform the background processing
        protected override fun doInBackground(vararg aparams: Parameters): Long {
            return doSoundTouchProcessing(aparams[0])
        }



    }
    //</editor-fold>

}