////////////////////////////////////////////////////////////////////////////////
package com.textapp.musicmuniapp.soundtouch

class SoundTouch {

    private external fun setTempo(handle: Long, tempo: Float)
    private external fun setPitchSemiTones(handle: Long, pitch: Float)
    private external fun setSpeed(handle: Long, speed: Float)
    private external fun processFile(handle: Long, inputFile: String, outputFile: String): Int
    private external fun deleteInstance(handle: Long)

    var handle: Long = 0

    fun close() {
        deleteInstance(handle)
        handle = 0
    }

    fun setTempo(tempo: Float) {
        setTempo(handle, tempo)
    }

    fun setPitchSemiTones(pitch: Float) {
        setPitchSemiTones(handle, pitch)
    }

    fun setSpeed(speed: Float) {
        setSpeed(handle, speed)
    }

    fun processFile(inputFile: String, outputFile: String): Int {
        return processFile(handle, inputFile, outputFile)
    }

    external fun getVersionString(): String
    external fun newInstance(): Long

    companion object {
        // Native interface function that returns SoundTouch version string.
        // This invokes the native c++ routine defined in "soundtouch-jni.cpp".

//        val versionString: String?
//            external get

        val errorString: String?
            external get

        // Load the native library upon startup
//        init {
//
//        }

    }

    init {
        System.loadLibrary("soundtouch")
        handle = newInstance()
    }

}